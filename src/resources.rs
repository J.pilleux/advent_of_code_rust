use std::{fs::File, io::Read, process};

pub fn get_input(year: i16, day: i16) -> String {
    let path = format!("resources/{}/day{:0>2}.input", year, day);
    let mut fd = match File::open(&path) {
        Ok(fd) => fd,
        Err(err) => {
            eprintln!("Cannot open file {} because of {}", &path, err);
            process::exit(1);
        }
    };

    let mut buf = String::new();
    match fd.read_to_string(&mut buf) {
        Ok(..) => {}
        Err(err) => eprintln!("Cannot read file {} because of {}", &path, err),
    };

    buf
}

pub fn get_lined_input(year: i16, day: i16) -> Vec<String> {
    let content = get_input(year, day);
    content.split("\n").map(String::from).collect()
}
