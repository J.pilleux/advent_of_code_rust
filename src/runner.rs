use crate::year_2021::year_runner::run_2021;

pub fn run(year: i16, day: i16) {
    match year {
        2021 => run_2021(year, day),
        _ => eprintln!("Year {} unknown", year)
    }
}
