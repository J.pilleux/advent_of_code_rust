use advent_of_code_rust::runner::run;
use args::parse_args;

mod args;

extern crate advent_of_code_rust;

fn main() {
    let opts = parse_args();
    run(opts.year, opts.day);
}
