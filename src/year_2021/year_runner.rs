use super::day_05::run_day05;

pub fn run_2021(year: i16, day: i16) {
    match day {
        5 => run_day05(year, day),
        _ => eprintln!("Day {} not implemented", day),
    }
}
