use crate::resources::get_input;

type Point = (i32, i32);

fn str_to_pts(str: String) -> Point {
    let ints: Vec<i32> = str.split(",").map(|p| {
        p.parse::<i32>().unwrap()
    }).collect();

    return (ints[0], ints[1])
}

fn split_arrow(line: String) -> (Point, Point) {
    let two_pts: Vec<Point> = line.split(" -> ").map(|p| {
        str_to_pts(p.to_string())
    }).collect();

    return (two_pts[0], two_pts[1])
}

pub fn run_day05(year: i16, day: i16) {
    let input = get_input(year, day);
    println!("{}", input);
}
