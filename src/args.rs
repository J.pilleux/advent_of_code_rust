use clap::Parser;

#[derive(Parser)]
#[clap(author, version, about)]
pub struct Cli {
    #[clap(short, long, value_parser, value_name = "YEAR")]
    pub year: i16,
    #[clap(short, long, value_parser, value_name = "DAY")]
    pub day: i16,
    #[clap(short, long, action)]
    pub is_hard: bool
}

pub fn parse_args() -> Cli {
    Cli::parse()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn verify_cli() {
        use clap::CommandFactory;
        Cli::command().debug_assert()
    }
}
